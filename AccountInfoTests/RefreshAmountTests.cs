﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountClassLib;
using Moq;
using System.Threading.Tasks;

namespace AccountInfoTests
{
    [TestClass]
    public class RefreshAmountTests
    {
        [TestMethod]
        public void RefreshAmount100()
        {
            //Кладем на счет 100, ожидаем получить на счету 100

            //arrange
            int setId = 1;
            var setAmount = 100;
            var expectedAmount = 100.0;

            var accountServiceMock = new Mock<IAccountService>();
            accountServiceMock.Setup(i => i.GetAccountAmount(setId))
                .Returns(Task.FromResult<double>(setAmount >= 0 ? setAmount : 0));

            var accInfo = new AccountInfo(setId, accountServiceMock.Object);


            //act
            accInfo.RefreshAmount();

            //assert
            Assert.AreEqual(expectedAmount, accInfo.Amount);
        }

        [TestMethod]
        public void RefreshAmountMinus100()
        {
            //Кладем на счет -100, ожидаем получить на счету 0 (т.к. не счету не может быть отрицательного значения)

            //arrange
            int setId = 1;
            var expectedAmount = 0;
            var setAmount = -100;


            var accountServiceMock = new Mock<IAccountService>();

            accountServiceMock.Setup(i => i.GetAccountAmount(setId))
                .Returns(Task.FromResult<double>(setAmount >= 0 ? setAmount : 0));

            var accInfo = new AccountInfo(setId, accountServiceMock.Object);


            //act
            accInfo.RefreshAmount();

            //assert
            Assert.AreEqual(expectedAmount, accInfo.Amount);
        }
    }
}
